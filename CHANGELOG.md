# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## [1.1.0](https://gitlab.com/addison.wanlass/concom/compare/v1.0.0...v1.1.0) (2020-12-17)


### Features

* **home:** Add new content to home page ([4944a1f](https://gitlab.com/addison.wanlass/concom/commit/4944a1f68ef2e9819d0377b53fe5ac56fba3acaa)), closes [#32](https://gitlab.com/addison.wanlass/concom/issues/32)

## [1.0.0](https://gitlab.com/addison.wanlass/concom/compare/v0.1.4...v1.0.0) (2020-12-17)

### [0.1.4](https://gitlab.com/addison.wanlass/concom/compare/v0.1.3...v0.1.4) (2020-12-17)


### Bug Fixes

* **home:** Fix broken import of home component ([722fdf3](https://gitlab.com/addison.wanlass/concom/commit/722fdf3c29c195c935e538c5a59f62e1fa5fd7fc)), closes [#25](https://gitlab.com/addison.wanlass/concom/issues/25)

### [0.1.3](https://gitlab.com/addison.wanlass/concom/compare/v0.1.2...v0.1.3) (2020-12-17)


### Features

* **home:** Add home component to App.js ([53eb0dc](https://gitlab.com/addison.wanlass/concom/commit/53eb0dcd9079a7c03e130f23a465ba13a080d91d)), closes [#4](https://gitlab.com/addison.wanlass/concom/issues/4) [#5](https://gitlab.com/addison.wanlass/concom/issues/5)

### [0.1.2](https://gitlab.com/addison.wanlass/concom/compare/v0.1.1...v0.1.2) (2020-12-17)

### 0.1.1 (2020-12-17)
